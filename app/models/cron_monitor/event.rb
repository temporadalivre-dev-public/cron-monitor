module CronMonitor
  
  class Event < ApplicationRecord

    belongs_to :category

    serialize :data, JSON

    enum kind: {
      start: 1,
      finish: 2
    }

    before_save :ensure_id_hash

    private

    def ensure_id_hash

      self.uuid ||= SecureRandom.uuid

    end

  end

end
