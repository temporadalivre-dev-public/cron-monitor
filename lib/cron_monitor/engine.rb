require 'turbolinks'
require 'slack-notifier'

module CronMonitor
  class Engine < ::Rails::Engine
    isolate_namespace CronMonitor
  end 
end
