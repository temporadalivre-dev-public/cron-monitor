require_dependency "cron_monitor/application_controller"

module CronMonitor
  class CategoriesController < ApplicationController
    before_action :set_category, only: [:edit, :update, :destroy]

    # GET /categories
    def index
      @categories = Category.all
      # @categories = Category.all
      @categories = CronMonitor::Category.left_joins(:events).select_with_last_start_and_finish_events_created_at.order("last_start_event_created_at desc")
    end

    # GET /categories/1
    def show
      @category = Category.where(id: params[:id]).select_with_last_start_and_finish_events_created_at.take
      @events = @category.events.order(created_at: :desc)
    end

    # GET /categories/new
    def new
      @category = Category.new
    end

    # GET /categories/1/edit
    def edit
    end

    # POST /categories
    def create
      @category = Category.new(category_params)

      if @category.save
        redirect_to @category, notice: 'Category was successfully created.'
      else
        render :new
      end
    end

    # PATCH/PUT /categories/1
    def update
      if @category.update(category_params)
        redirect_to @category, notice: 'Category was successfully updated.'
      else
        render :edit
      end
    end

    # DELETE /categories/1
    def destroy
      @category.destroy
      redirect_to categories_url, notice: 'Category was successfully destroyed.'
    end

    private
      # Use callbacks to share common setup or constraints between actions.
      def set_category
        @category = Category.find(params[:id])
      end

      # Only allow a trusted parameter "white list" through.
      def category_params
        params.require(:category).permit(:title, :description, :time_interval, :time_frame)
      end
  end
end
