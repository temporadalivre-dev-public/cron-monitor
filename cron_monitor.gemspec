$:.push File.expand_path("lib", __dir__)

# Maintain your gem's version:
require "cron_monitor/version"

# Describe your gem and declare its dependencies:
Gem::Specification.new do |spec|
  spec.name        = "cron_monitor"
  spec.version     = CronMonitor::VERSION
  spec.authors     = ["TemporadaLivre"]
  spec.email       = ["contato@temporadalivre.com"]
  spec.homepage    = "https://gitlab.com/temporadalivre-dev-public/cron-monitor"
  spec.summary     = "CRON Monitor"
  spec.description = "CRON Monitor"
  spec.license     = "MIT"

  # Prevent pushing this gem to RubyGems.org. To allow pushes either set the 'allowed_push_host'
  # to allow pushing to a single host or delete this section to allow pushing to any host.
  if spec.respond_to?(:metadata)
    spec.metadata["allowed_push_host"] = "TODO: Set to 'http://mygemserver.com'"
  else
    raise "RubyGems 2.0 or newer is required to protect against " \
      "public gem pushes."
  end

  spec.files = Dir["{app,config,db,lib}/**/*", "MIT-LICENSE", "Rakefile", "README.md"]

  spec.add_dependency "rails"
  spec.add_dependency "turbolinks"
  spec.add_dependency "slack-notifier"

  spec.add_development_dependency "pry"
  spec.add_development_dependency "sqlite3"
  spec.add_development_dependency "pry-rails"
  spec.add_development_dependency "marginalia"
end
