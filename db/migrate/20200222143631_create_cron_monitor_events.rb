class CreateCronMonitorEvents < ActiveRecord::Migration[5.0]
  def change
    create_table :cron_monitor_events do |t|
      t.references :category
      t.integer :kind
      t.text :data

      t.timestamps
    end
  end
end
