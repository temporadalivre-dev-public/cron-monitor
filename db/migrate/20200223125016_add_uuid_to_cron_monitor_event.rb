class AddUuidToCronMonitorEvent < ActiveRecord::Migration[5.0]
  def change
    add_column :cron_monitor_events, :uuid, :string
    add_index :cron_monitor_events, :uuid
  end
end
