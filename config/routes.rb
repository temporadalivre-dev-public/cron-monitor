CronMonitor::Engine.routes.draw do
  root to: "categories#index"
  resources :events
  resources :categories
end
