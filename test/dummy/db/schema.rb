# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# This file is the source Rails uses to define your schema when running `rails
# db:schema:load`. When creating a new database, `rails db:schema:load` tends to
# be faster and is potentially less error prone than running all of your
# migrations from scratch. Old migrations may fail to apply correctly if those
# migrations use external dependencies or application code.
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2020_02_23_125016) do

  create_table "cron_monitor_categories", force: :cascade do |t|
    t.string "title"
    t.text "description"
    t.integer "time_interval"
    t.integer "time_frame"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["description"], name: "index_cron_monitor_categories_on_description"
    t.index ["title"], name: "index_cron_monitor_categories_on_title", unique: true
  end

  create_table "cron_monitor_events", force: :cascade do |t|
    t.integer "category_id"
    t.integer "kind"
    t.text "data"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "uuid"
    t.index ["category_id"], name: "index_cron_monitor_events_on_category_id"
    t.index ["uuid"], name: "index_cron_monitor_events_on_uuid"
  end

end
