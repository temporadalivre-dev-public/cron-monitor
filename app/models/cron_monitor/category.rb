module CronMonitor
  class Category < ApplicationRecord
    
    enum time_frame: {
      minute: 1,
      hour: 2,
      day: 3,
      week: 4,
      month: 5,
      year: 6
    }

    has_many :events, dependent: :delete_all

    validate :validate_time_interval_and_frame

    def self.select_with_last_start_and_finish_events_created_at
      all.select("cron_monitor_categories.*, (SELECT MAX(cron_monitor_events.created_at) FROM cron_monitor_events WHERE cron_monitor_events.category_id = cron_monitor_categories.id AND cron_monitor_events.kind = 1) as last_start_event_created_at, (SELECT MAX(cron_monitor_events.created_at) FROM cron_monitor_events WHERE cron_monitor_events.category_id = cron_monitor_categories.id AND cron_monitor_events.kind = 2) as last_finish_event_created_at").group("cron_monitor_categories.id")
    end

    def check_alarm

      return if !has_alarm?

      notify_failure_alarms if !is_running_properly?

    end

    def notify_start_or_finish(kind: , data: nil)

      data_in_string = data.map{|k,v| "#{k}: #{v}"}.join(" | ") if data.present?

      CronMonitor.alarm_notifiers.each do |notifier|

        notifier.new(I18n.translate("cron_monitor.starting_or_finishing.#{kind}", title: self.title, datetime: Time.current.to_s, data: data_in_string, locale: CronMonitor.i18n), kind: kind).perform!

      end

    end

    def has_alarm?

      self.time_frame.present? && self.time_interval.present?

    end

    def is_running_properly?(last_finish_event_created_at: self.events.finish.last.try(:created_at))

      raise ArgumentError.new("This monitor doesn't have any alarm") unless has_alarm?

      if !should_have_run_yet? || (last_finish_event_created_at && last_finish_event_created_at > should_have_finished_after)

        true

      else

        false

      end

    end

    def should_have_run_yet?

      should_have_finished_after > self.created_at

    end

    def should_have_finished_after

      should_have_finished_after = self.time_interval.send(self.time_frame.to_sym).ago

    end

    private

      def notify_failure_alarms

        CronMonitor.alarm_notifiers.each do |notifier|

          notifier.new(failure_message, kind: :failure).perform!

        end

      end

      def failure_message

        message = I18n.translate(
          "cron_monitor.starting_or_finishing.failure",
          title: self.title,
          should_have_run: I18n.translate(
            "cron_monitor.starting_or_finishing.should_have_run",
            should_have_run_after: I18n.localize(self.should_have_finished_after),
            ran_at_or_never_ran: self.events.finish.last.try(:created_at).blank? ? I18n.translate("cron_monitor.starting_or_finishing.it_never_ran") : I18n.translate("cron_monitor.starting_or_finishing.last_ran_at", datetime: I18n.localize(self.events.finish.last.try(:created_at).to_datetime.in_time_zone)),
            locale: CronMonitor.i18n
          ),
          locale: CronMonitor.i18n
        )

      end

      def validate_time_interval_and_frame

        if (self.time_frame.present? && self.time_interval.blank?) || (self.time_frame.blank? && self.time_interval.present?)

          self.errors.add(:base, "Time frame and time interval must both be informed (or none)")

        end

      end

  end
end
