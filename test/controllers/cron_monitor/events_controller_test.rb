require 'test_helper'

module CronMonitor
  class EventsControllerTest < ActionDispatch::IntegrationTest
    include Engine.routes.url_helpers

    setup do
      @event = cron_monitor_events(:one)
    end

    test "should get index" do
      get events_url
      assert_response :success
    end

    test "should get new" do
      get new_event_url
      assert_response :success
    end

    test "should create event" do
      assert_difference('Event.count') do
        category = cron_monitor_categories(:one)

        params = {
          event: {
            category_id: category.id,
            data: {
              random_data: "New event random data #{rand(0..9999)}"
            }.to_json
          }
        }

        post events_url, params: params
      end

      assert_redirected_to event_url(Event.last)
    end

    test "should show event" do
      get event_url(@event)
      assert_response :success
    end

    test "should get edit" do
      get edit_event_url(@event)
      assert_response :success
    end

    test "should update event" do
      params = {
        event: {
          data: {
            random_data: "Just some random data #{rand(0..9999)}"
          }.to_json
        }
      }

      patch event_url(@event), params: params

      assert_redirected_to event_url(@event)
    end

    test "should destroy event" do
      assert_difference('Event.count', -1) do
        delete event_url(@event)
      end

      assert_redirected_to events_url
    end
  end
end
