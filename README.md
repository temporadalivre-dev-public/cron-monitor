# CronMonitor
Short description and motivation.

## Installation
Add this line to your application's Gemfile:

```ruby
gem 'cron_monitor'
```

And then execute:
```bash
$ bundle
```

Then run:

`rails cron_monitor:install:migrations`

And

`rails db:migrate`

After this, in an initializer, setup:

```
CronMonitor::Notifiers::SlackNotifier.slack_webhooks_endpoint = ENV.fetch('SLACK_WEBHOOKS_ENDPOINT')
CronMonitor::Notifiers::SlackNotifier.slack_webhooks_failure_channel = '@temporadalivre'
CronMonitor::Notifiers::SlackNotifier.slack_webhooks_start_and_finish_channel = '@temporadalivre'
```

Finally, add to you schedule.rb file:

```
every 5.minutes do
  rake 'cron_monitor:check_alarms'
end
```

## Usage

Inside, let's say, a rake task (or anywhere in your Rails app you desire):

```
CronMonitor.perform_with_monitor('Arbitrary name of your monitor') do |monitor|

  # Optional
  monitor.set_start_data(
    can_i_save_anything_here: 'yes, as you wish', is_calling_set_start_data_mandatory: 'not at all',
    arbitrary_start_data: 'yes please'
  )

  # Mandatory
  monitor.perform! do

    p "This is where your code gets run code "

  end

  # Optional
  monitor.set_finish_data(
    arbitrary_finish_data: 'yes please',
    can_i_save_anything_here: 'yes', is_calling_set_finish_data_mandatory: 'not at all'
  )

end

```

Options when you invoke `perform_with_monitor`, with their defaults:

- notify_only_on_failure: false, 
- delete_events_older_than: CronMonitor.delete_events_older_than

Example: 

```
CronMonitor.perform_with_monitor('Arbitrary name of your monitor', notify_only_on_failure: true, delete_events_older_than: 5.minutes) do |monitor|

  xxx

end

```

## Settings and defaults (you can override in an initializer):

```
CronMonitor::Notifiers::SlackNotifier.slack_webhooks_endpoint = ENV['SLACK_WEBHOOKS_ENDPOINT']

CronMonitor::Notifiers::SlackNotifier.slack_webhooks_failure_channel = nil

CronMonitor::Notifiers::SlackNotifier.slack_webhooks_start_and_finish_channel = nil
```

## Testing notifications in console

```
CronMonitor::Notifiers::SlackNotifier.slack_webhooks_endpoint = "https://hooks.slack.com/services/xxx/xxx/xxx"

CronMonitor::Notifiers::SlackNotifier.slack_webhooks_failure_channel = '@temporadalivre'
CronMonitor::Notifiers::SlackNotifier.slack_webhooks_start_and_finish_channel = '@temporadalivre'
```

## Contributing
Contribution directions go here.

## License
The gem is available as open source under the terms of the [MIT License](https://opensource.org/licenses/MIT).
