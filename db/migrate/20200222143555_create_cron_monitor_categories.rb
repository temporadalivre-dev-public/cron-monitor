class CreateCronMonitorCategories < ActiveRecord::Migration[5.0]
  def change
    create_table :cron_monitor_categories do |t|
      t.string :title
      t.text :description
      t.integer :time_interval
      t.integer :time_frame

      t.timestamps
    end
    add_index :cron_monitor_categories, :title, unique: true
  end
end
