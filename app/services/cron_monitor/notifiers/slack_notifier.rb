require_relative "base_notifier.rb"

module CronMonitor
  module Notifiers
    class SlackNotifier < CronMonitor::Notifiers::BaseNotifier

      class << self
        attr_accessor :slack_webhooks_endpoint,
                      :slack_webhooks_failure_channel,
                      :slack_webhooks_start_and_finish_channel
      end

      self.slack_webhooks_endpoint = ENV.fetch('SLACK_WEBHOOKS_ENDPOINT', nil)

      def initialize(message, kind: )

        raise ArgumentError.new("You must provide a slack webhooks endpoint") unless self.class.slack_webhooks_endpoint.present?
        raise ArgumentError.new("You must provide a slack start and finish channel") unless self.class.slack_webhooks_start_and_finish_channel.present?
        raise ArgumentError.new("You must provide a slack failure channel") unless self.class.slack_webhooks_failure_channel.present?
        @message = message
        @kind = kind
      end

      def perform!

        channel = @kind == :failure ? self.class.slack_webhooks_failure_channel : self.class.slack_webhooks_start_and_finish_channel

        Slack::Notifier.new(self.class.slack_webhooks_endpoint)
          .ping(@message, channel: channel, attachments: @attachments)

      end

    end
  end
end