require "cron_monitor/engine"

require CronMonitor::Engine.root.join("app/services/cron_monitor/notifiers/slack_notifier.rb")

module CronMonitor

  class << self

    attr_accessor :delete_events_older_than,
                  :alarm_notifiers,
                  :notify_only_on_failure,
                  :i18n

  end

  self.delete_events_older_than = 14.days

  self.notify_only_on_failure = false

  self.alarm_notifiers = [
    CronMonitor::Notifiers::SlackNotifier
  ]

  self.i18n = I18n.default_locale

  def self.track(event_name, kind: , **options)

    data = options.fetch(:data, nil)

    category = CronMonitor::Category.find_or_create_by!(title: event_name)

    # Inicialize variable so it can be used inside and outside block.
    event = nil

    category.with_lock do

      # We'll delete events older than the `delete_events_older_than` setting,
      # but we should never delete the start event corresponding to this end
      # event even if it's older than the setting
      category.events.where("created_at < ?", options[:delete_events_older_than].ago).where.not(uuid: options.fetch(:start_event_uuid, nil)).delete_all if options[:delete_events_older_than]

      event = category.events.create!(data: data, kind: kind)

    end


    unless options.fetch(:notify_only_on_failure, self.notify_only_on_failure)

      category.notify_start_or_finish(data: data, kind: kind)

    end

    event

  end

  def self.perform_with_monitor(event_name, **options, &block)

    data_store = DataStore.new(event_name, options)

    block.call(data_store)

    raise StandardError.new("You must call monitor.perform! in your code") if !data_store.called_perform

    CronMonitor.track(
      event_name,
      kind: :finish,
      data: data_store.finish_data,
      start_event_uuid: data_store.start_event_uuid,
      **options.merge!({ delete_events_older_than: options.fetch(:delete_events_older_than, CronMonitor.delete_events_older_than) })
    )

  end

  def self.check_alarms

    CronMonitor::Category.where.not(time_frame: nil).where.not(time_interval: nil).find_each do |category|

      category.with_lock do

        category.check_alarm

      end

    end

  end

  class DataStore

    attr_accessor :start_data, :finish_data, :called_perform, :start_event_uuid

    def initialize(event_name, options)

      @event_name = event_name
      @options = options

    end

    def set_finish_data(finish_data)

      self.finish_data = finish_data

    end

    def set_start_data(start_data)

      self.start_data = start_data

    end

    def perform!(&block)

      self.called_perform = true

      start_event = CronMonitor.track(
        @event_name,
        kind: :start,
        data: self.start_data,
        # When we start tracking an event, we still don't want to delete older
        # events, only after the code has run.
        **@options.except(:delete_events_older_than)
      )

      # We set the start_event_uuid so we never delete it on the end of a
      # perform block even tough it's creation is older than the
      # delete_events_older_than.
      self.start_event_uuid = start_event.uuid

      block.call

    end

  end

end
