module CronMonitor
  class ApplicationController < ActionController::Base
    
    protect_from_forgery with: :exception

    around_action :switch_locale
     
    private

    def switch_locale(&action)
      locale = CronMonitor.i18n || I18n.default_locale
      I18n.with_locale(locale, &action)
    end

  end
end
