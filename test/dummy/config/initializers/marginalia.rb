if Rails.env.development?
  Marginalia::Comment.components = [:application, :controller, :action, :line]
end
